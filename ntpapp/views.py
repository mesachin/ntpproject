from django.contrib.syndication.views import Feed
from django.urls import reverse, reverse_lazy
from django.core import serializers
from django.http import HttpResponse, JsonResponse
from django.views.generic import *
from .models import *
from .forms import *


class ClientMixin(object):
    def get_context_data(self, **kwargs):
        print("It should be just after middleware")
        context = super().get_context_data(**kwargs)
        context['allcategory'] = Category.objects.all()
        context['allblogs'] = Blog.objects.order_by('-id')

        return context


class ClientGetBlogListView(View):
    def get(self, request, *args, **kwargs):
        blogs = Blog.objects.order_by('-id')
        jsondata = serializers.serialize('json', blogs)
        return HttpResponse(jsondata)


class ClientFestivalListView(ListView):
    template_name = "clienttemplates/clientfestivallist.html"
    queryset = Festival.objects.all()
    context_object_name = "festivals"


class ClientFestivalDetailView(DetailView):
    template_name = "clienttemplates/clientfestivaldetail.html"
    queryset = Festival.objects.all()
    context_object_name = "festival"


class ClientHeritageListView(ListView):
    template_name = "clienttemplates/clientheritagelist.html,clientfestivaldetail.html"
    queryset = Heritage.objects.all()
    context_object_name = "Heritage"



class ClientHeritageDetailView(DetailView):
    template_name = "clienttemplates/clientheritagedetail.html"
    queryset = Heritage.objects.all()
    context_object_name = "Heritage"





class ClientBaseView(ClientMixin, TemplateView):
    template_name = "clienttemplates/clientbase.html"


class ClientHomeView(ClientMixin, TemplateView):
    template_name = "clienttemplates/clienthome.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['sliders'] = ImageSlider.objects.all()
        context['blogs'] = Blog.objects.order_by('-id')
        context['categorys'] = Category.objects.order_by('-id')
        return context


class ClientAboutView(ClientMixin, TemplateView):
    template_name = "clienttemplates/clientabout.html"


class ClientContactView(ClientMixin, TemplateView):
    template_name = "clienttemplates/clientContact.html"


class ClientBlogDetailView(ClientMixin, DetailView):
    template_name = "clienttemplates/clientblogdetail.html"
    model = Blog
    context_object_name = "blog"


class ClientGallaryListView(ClientMixin, ListView):
    template_name = "clienttemplates/clientgallarylist.html"
    model = Gallary
    context_object_name = "gallary"


class ClientCategoryDetailView(ClientMixin, DetailView):
    template_name = "clienttemplates/clientcategorydetail.html"
    # model = Category
    queryset = Category.objects.all()
    # queryset = Heritage.objects.filter(heritage_type="Natural Beauty")
    context_object_name = "category"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.exclude(id=self.object.id)
        return context


class LatestEntriesFeed(Feed):
    title = "Police beat site news"
    link = "/feed/"
    description = "Updates on changes and additions to police beat central."

    def items(self):
        return Blog.objects.order_by('-id')[:5]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return item.details

    # item_link is only needed if NewsItem has no get_absolute_url method.
    def item_link(self, item):
        return reverse('ntpapp:clientblogdetail', args=[item.pk])


class NaturalBeautyView(ClientMixin, TemplateView):
    template_name = "clienttemplates/naturalbeauty.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['naturalbeautys'] = Heritage.objects.filter(heritage_type="Natural Beauty")
        return context