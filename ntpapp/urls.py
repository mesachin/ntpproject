from django.urls import path
from .views import *


app_name = "ntpapp"
urlpatterns = [
    path('base_layout/', ClientBaseView.as_view(), name="clientbase"),
    path('get-blogs/', ClientGetBlogListView.as_view(), name="clientgetbloglist"),
    path('', ClientHomeView.as_view(), name="clienthome"),
    path('about/', ClientAboutView.as_view(), name="clientabout"),
    path('contact/', ClientContactView.as_view(), name="clientcontact"),

    path('our-blog/<int:pk>/detail/', ClientBlogDetailView.as_view(),
         name="clientblogdetail"),
    path('category/<int:pk>/detail/', ClientCategoryDetailView.as_view(),
         name="clientcategorydetail"),

    path('festival/', ClientFestivalListView.as_view(),
         name="clientfestivallist"),

    path('heritage/detail/',ClientHeritageListView.as_view(),
         name="clientheritagelist"),

    path('festival/<int:pk>/', ClientFestivalDetailView.as_view(),
         name="clientfestivaldetail"),

    path('gallary/', ClientGallaryListView.as_view(),
         name="clientgallarylist"),

    path('feed/', LatestEntriesFeed()),
    path('natural-beauty/', NaturalBeautyView.as_view(), name="naturalbeauty"),

]
